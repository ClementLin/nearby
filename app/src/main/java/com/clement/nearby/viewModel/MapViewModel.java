package com.clement.nearby.viewModel;

import android.arch.lifecycle.ViewModel;
import android.location.Location;
import android.util.Log;

import com.clement.nearby.data.DataModel;
import com.clement.nearby.data.NearbySearchConst;
import com.clement.nearby.data.NearbyType;
import com.clement.nearby.data.model.nearby.NearbySearchResponse;
import com.clement.nearby.utils.SingleLiveEvent;
import com.mapbox.android.core.location.LocationEngineListener;

import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class MapViewModel extends ViewModel implements LocationEngineListener, NearbySearchConst {

    public enum STATE{
        NORMAL, LOCATE, SEARCHING, SUCCESS, FAIL, OVER_QUERY
    }

    private static final String TAG = "MapViewModel";
    private DataModel dataModel;
    private String apiKey;
    private Location currentLocation;
    private STATE currentState = STATE.NORMAL;

    private SingleLiveEvent<STATE> onStateChangeEvent = new SingleLiveEvent<>();
    private SingleLiveEvent<Location> onLocationChanged = new SingleLiveEvent<>();
    private SingleLiveEvent<NearbySearchResponse> onSearchSuccess = new SingleLiveEvent<>();
    private SingleLiveEvent<Void> clearAllMarkerEvent = new SingleLiveEvent<>();

    public Location getCurrentLocation() {
        return currentLocation;
    }

    public STATE getCurrentState() {
        return currentState;
    }

    public SingleLiveEvent<Void> getClearAllMarkerEvent() {
        return clearAllMarkerEvent;
    }

    public SingleLiveEvent<STATE> getOnStateChangeEvent() {
        return onStateChangeEvent;
    }

    public SingleLiveEvent<Location> getOnLocationChanged() {
        return onLocationChanged;
    }

    public SingleLiveEvent<NearbySearchResponse> getOnSearchSuccess() {
        return onSearchSuccess;
    }

    public MapViewModel(DataModel dataModel) {
        this.dataModel = dataModel;
    }

    public void setApiKey(String apiKey){
        this.apiKey = apiKey;
    }


    public void searchBy(NearbyType type){
        Log.i(TAG, "searchBy: "+type.toString());
        clearAllMarkerEvent.call();

        updateState(STATE.SEARCHING);
        dataModel.searchNearbyByType(currentLocation,
                RADIUS,
                type,
                apiKey)
                .timeout(10, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        nearbySearchResponse -> {
                            if(nearbySearchResponse.getStatus().equalsIgnoreCase("over_query_limit")) {
                                updateState(STATE.OVER_QUERY);
                                return;
                            }
                            onSearchSuccess.setValue(nearbySearchResponse);
                            updateState(STATE.SUCCESS);
                            Log.i(TAG, "Search success!");

                        },
                        ex -> Log.i("ClementDebug", "getNearbyInfoByType fail: "+ex.getMessage()),
                        () -> updateState(STATE.NORMAL)
                );


    }

    private void updateState(STATE updateState){
        this.currentState = updateState;
        onStateChangeEvent.setValue(updateState);
    }

    @Override
    public void onConnected() {

    }

    @Override
    public void onLocationChanged(Location location) {
        this.currentLocation = location;
        onLocationChanged.setValue(location);
    }
}
