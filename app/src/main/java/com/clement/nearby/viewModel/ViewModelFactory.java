package com.clement.nearby.viewModel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.clement.nearby.data.DataModel;
import com.clement.nearby.data.NearbyModel;


public class ViewModelFactory implements ViewModelProvider.Factory {
    private final DataModel mDataSource;
    private static volatile ViewModelFactory INSTANCE;


    public static ViewModelFactory getInstance() {

        if (INSTANCE == null) {
            synchronized (ViewModelFactory.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ViewModelFactory(NearbyModel.getInstance());
                }
            }
        }
        return INSTANCE;
    }

    public ViewModelFactory(DataModel dataSource) {
        this.mDataSource = dataSource;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(MapViewModel.class)) {
            return (T) new MapViewModel(mDataSource);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
