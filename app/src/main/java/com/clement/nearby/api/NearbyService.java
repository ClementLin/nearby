package com.clement.nearby.api;

import com.clement.nearby.data.model.nearby.NearbySearchResponse;

import io.reactivex.Observable;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface NearbyService {

    @POST("json")
    Observable<NearbySearchResponse> getNearbyInfoByType(@Query("location") String location,
                                                         @Query("radius") String radius,
                                                         @Query("type") String type,
                                                         @Query("key") String apiKey);

}
