package com.clement.nearby.api;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NearbyApiManager {

    private static final String API_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/";

    private static NearbyApiManager mInstance = new NearbyApiManager();
    private NearbyService nearbyService;

    private NearbyApiManager() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        nearbyService = retrofit.create(NearbyService.class);
    }

    public static NearbyService getApi(){
        return mInstance.nearbyService;
    }
}
