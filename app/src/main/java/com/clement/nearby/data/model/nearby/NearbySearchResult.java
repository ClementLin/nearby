package com.clement.nearby.data.model.nearby;

import com.clement.nearby.data.model.nearby.result.Geometry;
import com.clement.nearby.data.model.nearby.result.OpeningHours;
import com.google.gson.annotations.SerializedName;

public class NearbySearchResult {
    @SerializedName("geometry")
    private Geometry geometry;

    @SerializedName("name")
    private String name;

    @SerializedName("vicinity")
    private String vicinity;

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }
}
