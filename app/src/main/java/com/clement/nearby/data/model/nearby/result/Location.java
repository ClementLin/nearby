package com.clement.nearby.data.model.nearby.result;

import com.google.gson.annotations.SerializedName;
import com.mapbox.mapboxsdk.geometry.LatLng;

import java.io.Serializable;

public class Location implements Serializable {
    @SerializedName("lat")
    private double lat;

    @SerializedName("lng")
    private double lng;

    public Location() {

    }

    public Location(LatLng latLng) {
        this.lat = latLng.getLatitude();
        this.lng = latLng.getLongitude();
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
