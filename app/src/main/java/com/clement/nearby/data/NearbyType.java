package com.clement.nearby.data;

public enum NearbyType {
    COFFEE("coffee"), RESTAURANT("restaurant"), STORE("store");

    private final String value;

    private NearbyType(String value){
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
