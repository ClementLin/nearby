package com.clement.nearby.data;

import android.location.Location;

import com.clement.nearby.data.model.nearby.NearbySearchResponse;

import io.reactivex.Observable;

public interface DataModel {
    Observable<NearbySearchResponse> searchNearbyByType(Location location,
                                                        String radius,
                                                        NearbyType type,
                                                        String apiKey);
}
