package com.clement.nearby.data.model.nearby;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NearbySearchResponse {
    @SerializedName("results")
    private List<NearbySearchResult> results;

    @SerializedName("status")
    private String status;

    public List<NearbySearchResult> getResults() {
        return results;
    }

    public void setResults(List<NearbySearchResult> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
