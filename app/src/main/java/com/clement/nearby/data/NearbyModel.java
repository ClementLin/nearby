package com.clement.nearby.data;

import android.location.Location;

import com.clement.nearby.api.NearbyApiManager;
import com.clement.nearby.data.model.nearby.NearbySearchResponse;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class NearbyModel implements DataModel {

    private static NearbyModel INSTANCE;

    public static NearbyModel getInstance(){
        if (INSTANCE == null) {
            synchronized (NearbyModel.class) {
                if (INSTANCE == null) {
                    INSTANCE = new NearbyModel();
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public Observable<NearbySearchResponse> searchNearbyByType(Location location, String radius, NearbyType type, String apiKey) {
        StringBuffer locationInfo = new StringBuffer();
        locationInfo.append(location.getLatitude()).append(",").append(location.getLongitude());
        return NearbyApiManager.getApi().getNearbyInfoByType(locationInfo.toString(),
                radius,
                type.toString(),
                apiKey)
                .subscribeOn(Schedulers.io());
    }
}
