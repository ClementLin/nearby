package com.clement.nearby.data.model.nearby.result;

import com.google.gson.annotations.SerializedName;

public class Viewport {
    @SerializedName("northeast")
    private Location norteast;

    @SerializedName("southwest")
    private Location southwest;

    public Location getNorteast() {
        return norteast;
    }

    public void setNorteast(Location norteast) {
        this.norteast = norteast;
    }
}
