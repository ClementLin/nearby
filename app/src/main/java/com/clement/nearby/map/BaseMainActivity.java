package com.clement.nearby.map;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.clement.nearby.R;
import com.clement.nearby.utils.ToastUtil;

public class BaseMainActivity extends AppCompatActivity {

    private long pressBaceTime = -1;
    private boolean isExit(){
        long currentTime = System.currentTimeMillis();
        if (pressBaceTime == -1 || currentTime - pressBaceTime > 2000) {
            pressBaceTime = currentTime;
            ToastUtil.getInstance().showToast(this, R.string.press_back_again_to_close_app);
            return false;
        }

        return currentTime - pressBaceTime < 2000;
    }

    @Override
    public void onBackPressed() {
        if (isExit()) {
            setResult(RESULT_OK);
            finish();
            super.onBackPressed();
        }
    }
}
