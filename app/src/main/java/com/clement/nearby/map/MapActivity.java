package com.clement.nearby.map;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.clement.nearby.R;
import com.clement.nearby.data.model.nearby.NearbySearchResult;
import com.clement.nearby.databinding.ActivityMapBinding;
import com.clement.nearby.utils.AppUtils;
import com.clement.nearby.utils.PointUtils;
import com.clement.nearby.viewModel.MapViewModel;
import com.clement.nearby.viewModel.ViewModelFactory;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.mapbox.api.directions.v5.DirectionsCriteria;
import com.mapbox.api.directions.v5.models.DirectionsResponse;
import com.mapbox.api.directions.v5.models.DirectionsRoute;
import com.mapbox.core.constants.Constants;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.CameraMode;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.RenderMode;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
import com.mapbox.services.android.navigation.ui.v5.route.OnRouteSelectionChangeListener;
import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapActivity extends BaseMainActivity implements CameraPositionConst, PlaceSelectionListener, MapboxMap.OnInfoWindowClickListener, OnRouteSelectionChangeListener {

    private static final String TAG = "MapActivity";
    private ActivityMapBinding binding;
    private MapViewModel viewModel;
    private MapboxMap mapboxMap;
    private NavigationMapRoute mapRoute;
    private DirectionsRoute directionsRoute;

    private AtomicBoolean isNearbyMenuShowing = new AtomicBoolean(false);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");

        viewModel = obtainViewModel(this);
        viewModel.setApiKey(AppUtils.getAppMetaData(this, "com.google.android.geo.API_KEY"));

        binding = DataBindingUtil.setContentView(this, R.layout.activity_map);
        binding.setViewModel(viewModel);

        //init map
        binding.mapView.onCreate(savedInstanceState);

        binding.mapView.getMapAsync(mapboxMap -> {
            Log.i(TAG, "Mapbox map ready");
            this.mapboxMap = mapboxMap;
            mapboxMap.setOnInfoWindowClickListener(this);
            locate();
            initMapRoute();
        });

        //init Place autocomplete
        initAutoCompleteFragment();
        initObserveEvent();
        initOnClickEvent();

    }

    private void initMapRoute() {
        mapRoute = new NavigationMapRoute(binding.mapView, mapboxMap);
        mapRoute.setOnRouteSelectionChangeListener(this);
    }

    private void initAutoCompleteFragment() {
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(this);
    }

    private void initObserveEvent() {
        viewModel.getOnStateChangeEvent().observe(this, state -> {
            switch (state){
                case NORMAL:
                    break;
                case LOCATE:
                    break;
                case SUCCESS:
                    releaseSearchAnim();
                    break;
                case SEARCHING:
                    activateSearchAnim();
                    break;
                case FAIL:
                    releaseSearchAnim();
                    showErrorMsgDialog(getString(R.string.query_failed));
                    break;
                case OVER_QUERY:
                    releaseSearchAnim();
                    showErrorMsgDialog(getString(R.string.over_query_limit));
                    break;
            }
        });
        viewModel.getClearAllMarkerEvent().observe(this, aVoid -> clearAllMarker());

        viewModel.getOnLocationChanged().observe(this, location -> easeCameraPosition(location));

        viewModel.getOnSearchSuccess().observe(this, nearbySearchResponse -> {
            nearbySearchResponse.getResults().forEach(result -> addMarker(result));
        });
    }

    private void activateSearchAnim(){
        if (isNearbyMenuShowing.compareAndSet(true, false)){
            binding.nearbyMenuBtn.collapse();
            binding.fakeNearbyMenuBtn.setIcon(R.drawable.nearby_search);
        }
        binding.searchLoadingCircle.show();
    }

    private void releaseSearchAnim(){
        binding.searchLoadingCircle.hide();
    }

    private void initOnClickEvent() {
        if (viewModel.getCurrentState() == MapViewModel.STATE.SEARCHING)
            return;

        binding.fakeNearbyMenuBtn.setOnClickListener(view -> {
            if (isNearbyMenuShowing.compareAndSet(false, true)){
                binding.nearbyMenuBtn.expand();
                binding.fakeNearbyMenuBtn.setIcon(R.drawable.cancel);
            } else if (isNearbyMenuShowing.compareAndSet(true, false)){
                binding.nearbyMenuBtn.collapse();
                binding.fakeNearbyMenuBtn.setIcon(R.drawable.nearby_search);
            }
        });

        //init location btn
        binding.locationBtn.setOnClickListener(view -> {
            locate();
        });

        binding.naviMenuBtn.setOnClickListener(view -> {
            launchNavigationWithRoute();
        });
    }

    private void showErrorMsgDialog(String msg){
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, (dialogInterface, i) -> {})
                .create();

        dialog.show();
    }

    private void addMarker(NearbySearchResult nearbySearchResult){
        String title = nearbySearchResult.getName();
        String spippet = nearbySearchResult.getVicinity();
        LatLng latLng = new LatLng(nearbySearchResult.getGeometry().getLocation().getLat(), nearbySearchResult.getGeometry().getLocation().getLng());
        mapboxMap.addMarker(new MarkerOptions().position(latLng).title(title).setSnippet(spippet));
    }

    private void addMarker(Place place){
        LatLng latLng = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
        mapboxMap.addMarker(new MarkerOptions().position(latLng).title(place.getName().toString()).setSnippet(place.getAddress().toString()));
    }

    private void clearAllMarker(){
        binding.naviMenuBtn.setVisibility(View.GONE);
        mapRoute.removeRoute();
        mapboxMap.getMarkers().forEach(marker -> mapboxMap.removeMarker(marker));
    }

    public static MapViewModel obtainViewModel(FragmentActivity activity){
        ViewModelFactory factory = ViewModelFactory.getInstance();

        MapViewModel viewModel =
                ViewModelProviders.of(activity, factory).get(MapViewModel.class);

        return viewModel;
    }

    @Override
    public void onPlaceSelected(Place place) {
        Log.i(TAG, "Place Selected: " + place.getName());
        clearAllMarker();
        addMarker(place);
        easeCameraPosition(place);
        getRoute(PointUtils.toPoint(viewModel.getCurrentLocation()), PointUtils.toPoint(place));
    }

    @Override
    public void onError(Status status) {
        showErrorMsgDialog(getString(R.string.place_select_error));
    }

    private void getRoute(Point origin, Point destination){
        NavigationRoute.Builder builder = NavigationRoute.builder(this)
                .accessToken(Mapbox.getAccessToken())
                .origin(origin)
                .destination(destination)
                .alternatives(true);

        builder.build()
                .getRoute(new Callback<DirectionsResponse>() {
                    @Override
                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                        directionsRoute = response.body().routes().get(0);
                        if (directionsRoute.distance() > 25d){
                            mapRoute.addRoutes(response.body().routes());
                            boundCameraToRoute();
                            binding.naviMenuBtn.setVisibility(View.VISIBLE);
                        } else {
                            showErrorMsgDialog(getString(R.string.please_select_a_longer_route));
                        }
                    }

                    @Override
                    public void onFailure(Call<DirectionsResponse> call, Throwable t) {
                        showErrorMsgDialog(getString(R.string.getting_route_failed));

                    }
                });
    }

    private void launchNavigationWithRoute(){
        NavigationLauncherOptions.Builder optionsBuilder = NavigationLauncherOptions.builder()
                .shouldSimulateRoute(false)
                .directionsProfile(DirectionsCriteria.PROFILE_DRIVING_TRAFFIC);

        optionsBuilder.directionsRoute(directionsRoute);
        NavigationLauncher.startNavigation(MapActivity.this, optionsBuilder.build());
    }

    public void boundCameraToRoute() {
        if (directionsRoute != null) {
            List<Point> routeCoords = LineString.fromPolyline(directionsRoute.geometry(),
                    Constants.PRECISION_6).coordinates();
            List<LatLng> bboxPoints = new ArrayList<>();
            for (Point point : routeCoords) {
                bboxPoints.add(new LatLng(point.latitude(), point.longitude()));
            }
        }
    }

    private void easeCameraPosition(Place place){
        easeCameraPosition(place.getLatLng().latitude, place.getLatLng().longitude);
    }

    private void easeCameraPosition(Location location){
        easeCameraPosition(location.getLatitude(), location.getLongitude());
    }

    private void easeCameraPosition(double lat, double lng){
        if (mapboxMap == null)
            return;

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lat, lng))
                .zoom(ZOOM)
                .tilt(TILT)
                .build();

        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @SuppressLint({"MissingPermission", "WrongConstant"})
    private void locate(){
        Log.i(TAG, "locate: init");
        LocationComponent locationComponent = mapboxMap.getLocationComponent();
        locationComponent.activateLocationComponent(this);

        locationComponent.setLocationComponentEnabled(true);
        locationComponent.setRenderMode(RenderMode.NORMAL);
        locationComponent.setCameraMode(CameraMode.TRACKING);
        locationComponent.getLocationEngine().addLocationEngineListener(viewModel);
    }

    @Override
    public boolean onInfoWindowClick(@NonNull Marker marker) {
        Log.i("ClementDebug", "onInfoWindowClick: name = "+marker.getTitle());
        showNaviAskDialog(marker);
        return true;
    }

    private void showNaviAskDialog(Marker marker) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setMessage(R.string.navi_ask)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, ((dialogInterface, i) -> {
                    MarkerOptions markerOptions = new MarkerOptions().position(marker.getPosition()).title(marker.getTitle()).setSnippet(marker.getSnippet());
                    clearAllMarker();
                    mapboxMap.addMarker(markerOptions);
                    getRoute(PointUtils.toPoint(viewModel.getCurrentLocation()), PointUtils.toPoint(markerOptions.getPosition()));
                }))
                .setNegativeButton(R.string.cancel, ((dialogInterface, i) -> {}))
                .create();

        dialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        binding.mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        binding.mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding.mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        binding.mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onNewPrimaryRouteSelected(DirectionsRoute directionsRoute) {
        this.directionsRoute = directionsRoute;
    }
}
