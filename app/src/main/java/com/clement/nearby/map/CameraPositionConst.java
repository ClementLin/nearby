package com.clement.nearby.map;

public interface CameraPositionConst {
    int ZOOM = 15;
    int TILT = 20;
}
