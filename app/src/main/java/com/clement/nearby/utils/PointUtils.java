package com.clement.nearby.utils;

import android.location.Location;

import com.google.android.gms.location.places.Place;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.geometry.LatLng;

public class PointUtils {

    public static Point toPoint(Location location){
        return Point.fromLngLat(location.getLongitude(), location.getLatitude());
    }

    public static Point toPoint(Place location){
        return Point.fromLngLat(location.getLatLng().longitude, location.getLatLng().latitude);
    }

    public static Point toPoint(LatLng location){
        return Point.fromLngLat(location.getLongitude(), location.getLatitude());
    }


}
